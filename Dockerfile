FROM alpine:latest

MAINTAINER Martin Alker <martin.alker@mailbox.org>

RUN apk update \
    && apk upgrade \
    && apk add --no-cache \
	           bash \
	           git \
	           openssh-client \
			   ca-certificates \
	&& update-ca-certificates 2>/dev/null || true \
 	&& rm -rf /var/cache/apk/*

CMD ["/bin/bash"]
