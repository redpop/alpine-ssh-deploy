# A minimal Docker image based on Alpine Linux for deploying with SSH

## GitLab

### CI / CD

Documentation as Code 😁Please look into **.gitlab-ci.yml**!

GitLab pushes the build to their own registry and to Docker Hub! For Docker Hub you need an account or remove the `docker-hub:` block in **.gitlab-ci.yml**!

If you want to use Docker Hub then you have to add variables in your GitLab project > Settings > CI / CD > Environment variables!

#### Base64 Encoding

If you use private keys as GitLab environment variable and during deployment you get an error message like this

`Error loading key "(stdin)": invalid format`

then you have to encode your private key with a bease64 encoder. Here is a good one: [Base64 Encoder](https://opinionatedgeek.com/Codecs/Base64Encoder)
